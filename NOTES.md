# Notes

- https://github.com/SaraVieira/react-social-sharing
  - https://github.com/SaraVieira/react-social-sharing/blob/v3.3.0/src/consts.js
- https://faq.whatsapp.com/425247423114725/?cms_platform=iphone
- https://github.com/kytta/shareon
  - https://github.com/kytta/shareon/blob/v2.5.0/src/shareon.js#L20
  - https://shareon.js.org/
- https://core.telegram.org/api/links
  - "Deep links"
  - https://core.telegram.org/api/links#share-links
  - https://core.telegram.org/widgets/share
- https://en.wikipedia.org/wiki/Deep_linking
- https://developer.apple.com/ios/universal-links/
- https://en.wikipedia.org/wiki/Social_media
